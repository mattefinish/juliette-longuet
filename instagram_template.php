<?php
/*
Template Name: Instagram
*/

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content">
				<header class="page_header">
					<h2 class="center"><?php the_title(); ?></h2>
				</header>
				<div class="padding-bottom50">
					<div id="instafeed_page"></div>
					<a href="https://www.instagram.com/juliettelonguet/" target="_blank" class="uppercase black center">View More</a>
				</div>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
