<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	</div>

	<footer class="footer">
		<div class="footer_logo">
			<img src="<?php the_field('jl_black_logo','option'); ?>" />
		</div>
		<div class="footer_nav">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'footer-navigation', 'container' => '' ) ); ?>
		</div>
		<div class="footer_nav">
			<?php wp_nav_menu( array( 'menu' => 'social', 'menu_id' => 'footer-social-navigation', 'container' => '' ) ); ?>
		</div>
		<div class="footer_newsletter">
			<p class="header">Subscribe to our newsletter</p>

			<!-- Begin MailChimp Signup Form -->

			<div id="mc_embed_signup">
				<form action="//juliettelonguet.us6.list-manage.com/subscribe/post?u=61d378b51ffbb1ecf9d0e3ee4&amp;id=a5d1989648" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				    <div id="mc_embed_signup_scroll">
						<div class="mc-field-group">
							<input type="email" value="" name="EMAIL" placeholder="Your Email Address" class="required email" id="mce-EMAIL">
							<input type="submit" value="Send" name="subscribe" id="mc-embedded-subscribe" class="button">
						</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>    
					    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_bfaeb9f9cfe94bf88ebe82105_58d7ecad1c" tabindex="-1" value=""></div>
					</div>
				</form>
			</div>
		</div>
		<img src="<?php echo get_template_directory_uri() . '/img/arrow-up.svg'; ?>" class="back-to-top" />
	</footer>
</div>

<?php wp_footer(); ?>

<!-- Google Analytics Pulled from Options Page Created by ACF -->

<?php /* if(get_field('options_google_analytics','option')): ?>

	<script>
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php the_field('options_google_analytics','option'); ?>']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

<?php endif; */?>

</body>
</html>
