<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content">
				<main class="page_container">
					<div class="featured_image">
						<?php the_post_thumbnail(); ?>
					</div>
					<div class="text">
						<?php the_content(); ?>
					</div>
				</main>
				<?php get_template_part( 'template-parts/instagram' ); ?>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
