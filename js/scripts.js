var $j = jQuery.noConflict();

$j(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

	/*================================= 
	NEWS MENU ITEM CLICK FUNCTIONS
	=================================*/
	
	/*=== Smoothscroll to News ===*/

	$j('#primary-menu li.menu-item-171 a').smoothScroll({
	   offset: 0
	});

	$j('#mobile-menu li a').smoothScroll({
		offset: -50
	});

	/*=== Click on mobile navigation, fade out the mobile nav container ===*/

	$j('#mobile-menu li a').click(function(){
		$j('.mobile_navigation').fadeOut('fast');
	});

	/*================================= 
	HEADER NAVIGATION EFFECT
	=================================*/

	var lastScrollTop = 0;

	$j(window).scroll(function(event){

	   // Set the current scroll top

	   var st = $j(this).scrollTop();

	   // If the current scroll top is greater than the last then the user is scrolling down.
	   // Ensure the scroll position is greater than the height of the nav to fix mobile bug.

	   if (st > lastScrollTop && lastScrollTop > 40){
	       $j('header.site_header').addClass('scroll_down');
	       $j('.main_mobile_header header.mobile_header').addClass('scroll_down');
	   }

	   // If the user is at the top of the page reset the nav to normal

	   else if (st === 0){
	   	   $j('header.site_header').removeClass('scroll_up');
	   	   $j('.main_mobile_header header.mobile_header').removeClass('scroll_down');
	   } 

	   // Else the user is scrolling up so show the nav

	   else {
	       $j('header.site_header').removeClass('scroll_down').addClass('scroll_up');
	       $j('.main_mobile_header header.mobile_header').removeClass('scroll_down');
	   }

	   // Cache the current scroll top

	   lastScrollTop = st;
	});

	/*======================== 
	OVERLAY BOX HOVER EFFECT MOBILE
	========================*/

	$j('.jl_overlay').bind('touchstart touchend', function() {
        $j(this).toggleClass('hover_effect');
    });

	/*======================== 
	MOBILE NAVIGATION
	========================*/

	/*=== Open the fullscreen overlay ===*/

	$j('.menu_icon').click(function(){
	  $j('.mobile_navigation').fadeIn('fast');
	});

	/*=== Clicking the x within the overlay closes it ===*/
	
	$j('.close_menu_icon').click(function(){
	  $j('.mobile_navigation').fadeOut('fast');
	});

	/*================================= 
	BACK TO TOP
	=================================*/

	$j('.back-to-top').click(function(){
		$j('html, body').animate({
			scrollTop: 0
		}, 800);
	});

	/*================================= 
	MIXITUP
	Uses a custom load more plugin https://github.com/Trolleymusic/mixitup-loadmore
	=================================*/

	/*=== News Page mixItUp initialization ===*/

	$j('#news_mixitup').mixItUp({
	  loadmore: {
	    buttonClass: 'loadmore-button',
	    buttonLabel: 'Load More',
	    buttonWrapper: '.loadmore',
	    initial: 9,
	    lessClass: 'loadmore-less',
	    lessLabel: 'less',
	    loadMore: 9
	  },
	  load: {
		filter: '.tvshow'
	  }
	});

	/*=== Blog Page mixItUp initialization ===*/

	$j('#blog_mixitup').mixItUp({
	  loadmore: {
	    buttonClass: 'loadmore-button',
	    buttonLabel: 'Load More',
	    buttonWrapper: '.loadmore',
	    initial: 9,
	    lessClass: 'loadmore-less',
	    lessLabel: 'less',
	    loadMore: 9
	  }
	});

	/*================================= 
	INSTAFEED
	https://github.com/stevenschobert/instafeed.js
	=================================*/

	/*=== Footer Feed ===*/

	var feed = new Instafeed({
		clientId: '62b54a93bd0346e390bf0ca2e0423954',
		accessToken: '250425318.62b54a9.f5474a607ee241a08f3f4ce74d9a8ed7',
		template: '<div class="instafeed_item"><a href="{{link}}" target="_blank"><img src="{{image}}" /></a></div>',
        get: 'user',
        userId: '293404745',
        limit: 5,
        resolution: 'standard_resolution',
        error: function(error) {
        	console.log(error);
        }
    });

    if($j('#instafeed').length > 0) {
    	feed.run();
    }

    /*=== Instagram Page Feed ===*/

    var feedPage = new Instafeed({
    	target: 'instafeed_page',
		clientId: '62b54a93bd0346e390bf0ca2e0423954',
		accessToken: '250425318.62b54a9.f5474a607ee241a08f3f4ce74d9a8ed7',
		template: '<div class="instafeed_item"><a href="{{link}}" target="_blank"><img src="{{image}}" /></a></div>',
        get: 'user',
        userId: '293404745',
        limit: 20,
        resolution: 'standard_resolution',
        error: function(error) {
        	console.log(error);
        }
    });

    if($j('#instafeed_page').length > 0) {
    	feedPage.run();
    }

	/*================================= 
	SOCIAL SHARE
	=================================*/

	/*== Facebook Share ==*/

	$j('a.fb_share').click(function(e) {

		e.preventDefault();

		var loc = $j(this).attr('href');

		window.open('http://www.facebook.com/sharer.php?u=' + loc,'facebookwindow','height=450, width=550, top='+($j(window).height()/2 - 225) +', left='+$j(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

	});

	/*== Twitter Share ==*/

	$j('a.twitter_share').click(function(e){

	    e.preventDefault();

	    var loc = $j(this).attr('href');

	    var via = $j(this).attr('via');

	    var title  = encodeURIComponent($j(this).attr('title'));

	    window.open('http://twitter.com/share?url=' + loc, 'twitterwindow', 'height=450, width=550, top='+($j(window).height()/2 - 225) +', left='+$j(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

	});

	/*================================= 
	FITVIDS
	=================================*/

	/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

	$j('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

	/*=== fitVids.js ===*/

	$j(".video_embed").fitVids();

});
