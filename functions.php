<?php
/**
 * _s functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package _s
 */

if ( ! function_exists( '_s_setup' ) ) :

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _s_setup() {
	
	/*==========================================
	MAKE THEME AVAILABLE FOR TRANSLATION
	==========================================*/
	
	load_theme_textdomain( '_s', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.

	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*==========================================
	ENABLE SUPPORT FOR POST THUMBNAILS ON POSTS AND PAGES
	==========================================*/

	add_theme_support( 'post-thumbnails' );

	/*==========================================
	SETUP NAVIGATION MENUS
	==========================================*/

	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', '_s' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*==========================================
	ENABLE SUPPORT FOR POST FORMATS
	==========================================*/

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;

add_action( 'after_setup_theme', '_s_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _s_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );

/*==========================================
REMOVE WP EMOJI
==========================================*/

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/*==========================================
REGISTER WIDGET AREA
https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
==========================================*/

function _s_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );

/*==========================================
ENQUEUE SCRIPTS AND STYLES
==========================================*/

function _s_scripts() {
	
	// Default theme style

	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	// Wordpress Default Jquery
	
	if (!is_admin()) {
		wp_deregister_script( 'jquery' );
		wp_deregister_script('jquery-migrate');
    	wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), FALSE, NULL, TRUE);
		wp_register_script('jquery-migrate', includes_url('/js/jquery/jquery-migrate.min.js'), FALSE, NULL, TRUE);
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-migrate');
	}

	// Louize Display NP Font

	wp_enqueue_style('louizedisplaynp-font', get_template_directory_uri() . ('/fonts/LouizeDisplayNP/LouizeDisplayNP.css'), '','','');

	// Garamond Google Font

	wp_enqueue_style('garamond-font', 'https://fonts.googleapis.com/css?family=EB+Garamond', '','','');

	// Compiled SCSS File

	wp_enqueue_style( 'custom_styles', get_template_directory_uri() . '/css/style.min.css' );

	// FitVids

	wp_enqueue_script('_s-fitvids', get_template_directory_uri() . '/js/fitvids/fitvids.min.js', '', '', true);

	// Modernizr (Flexbox & Geolocation Detection)

	wp_enqueue_script( '_s-modernizr', get_template_directory_uri() . '/js/modernizr/modernizr-custom.min.js', '','', true);

	// Instafeed

	wp_enqueue_script( 'instafeed', get_template_directory_uri() . '/js/instafeed/instafeed.min.js', '','', true);

	// Mixitup Core

	wp_enqueue_script( 'mixitup', get_template_directory_uri() . '/js/mixitup/jquery.mixitup.min.js', '','', true);

	// Mixitup Load More

	wp_enqueue_script( 'mixitup_loadmore', get_template_directory_uri() . '/js/mixitup/jquery.mixitup-loadmore.min.js', '','', true);

	// Smooth Scroll

	wp_enqueue_script( 'smooth_scroll', get_template_directory_uri() . '/js/smoothscroll/smoothscroll.min.js', '','', true);

	// Custom Scripts

	wp_enqueue_script('_s-scripts', get_template_directory_uri() . '/js/scripts.min.js', '', '', true);

	wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}

add_action( 'wp_enqueue_scripts', '_s_scripts' );

/*==========================================
CUSTOM POST TYPES
==========================================*/

/*=== Case Studies ===*/

function custom_post_type_case_studies() {

	$labels = array(
		'name'                => ('Case Studies'),
		'singular_name'       => ('Case Study'),
		'menu_name'           => ('Case Studies'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Case Studies'),
		'view_item'           => ('View Case Study'),
		'add_new_item'        => ('Add New Case Study'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Case Study'),
		'update_item'         => ('Update Case Study'),
		'search_items'        => ('Search Case Studies'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Case Studies'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'excerpt'  ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'case-study'),
		'taxonomies'          => array( 'casestudy_category' ),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-id-alt',
	);

	register_post_type( 'jl_casestudies', $args );

}

add_action( 'init', 'custom_post_type_case_studies', 0 );

/*==========================================
CUSTOM TAXONOMY 
==========================================*/

/*=== Case Studies Taxonomy ===*/

function add_case_studies_taxonomy() {

  register_taxonomy('case_studies_category', 'jl_casestudies', array(

      'hierarchical' => true,
      'labels' => array(
      'name' => ('Category'),
      'singular_name' => ('Category'),
      'search_items' =>  ('Search Categories' ),
      'all_items' => ('All Categories' ),
      'parent_item' => ('Parent Category' ),
      'parent_item_colon' => ('Parent Category:' ),
      'edit_item' => ('Edit Category' ),
      'update_item' => ('Update Category' ),
      'add_new_item' => ('Add New Category' ),
      'new_item_name' => ('New Category Name' ),
      'menu_name' => ('Categories' ),
    ),

      'rewrite' => array(
      'slug' => 'type', 
      'with_front' => false,
      'hierarchical' => false
    ),
  ));
}
add_action( 'init', 'add_case_studies_taxonomy', 0 );

/*=============================================
ACF OPTIONS PAGE
=============================================*/

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

/*==========================================
LIMIT POST REVISIONS
==========================================*/

function limit_post_revisions( $num, $post ) {
    $num = 3;
    return $num;
}

add_filter( 'wp_revisions_to_keep', 'limit_post_revisions', 10, 2 );

/*=============================================
PAGE EXCERPTS
=============================================*/

function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

add_action( 'init', 'add_excerpts_to_pages' );

/*=============================================
CUSTOM LOGIN SCREEN
=============================================*/

// Change the login logo URL

function my_loginURL() {
    return 'http://loginurl.com';
}
add_filter('login_headerurl', 'my_loginURL');

// Enque the login specific stylesheet for design customizations. CSS file is compiled through compass.

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/css/login.min.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

/*=============================================
YOAST
=============================================*/

/*=== Adjust Metabox Priority

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

===*/

/*==========================================
SHORTCODES
==========================================*/

/*=== Row Shortcode ===*/

function jl_row_shortcode( $atts, $content = null ) {

	return '<div class="jl_row">' . do_shortcode($content) . '</div>';

}

add_shortcode( 'jl_row', 'jl_row_shortcode' );

/*=== Row w/ Single Image Shortcode ===*/

function jl_image_row_shortcode( $atts, $content = null ) {

	return '<div class="jl_image_row">' . do_shortcode($content) . '</div>';

}

add_shortcode( 'jl_image_row', 'jl_image_row_shortcode' );

/*=== Row w/ Centered Text Shortcode ===*/

function jl_center_text_row_shortcode( $atts, $content = null ) {

	return '<div class="jl_center_text_row">' . do_shortcode($content) . '</div>';

}

add_shortcode( 'jl_center_text_row', 'jl_center_text_row_shortcode' );

/*=== Half Shortcode ===*/

function jl_half_shortcode( $atts, $content = null ) {

	return '<div class="jl_half">' . do_shortcode($content) . '</div>';

}

add_shortcode( 'jl_half', 'jl_half_shortcode' );

/*=== Half Shortcode w/text ===*/

function jl_text_half_shortcode( $atts, $content = null ) {

	return '<div class="jl_text_half">' . do_shortcode($content) . '</div>';

}

add_shortcode( 'jl_text_half', 'jl_text_half_shortcode' );

/*==========================================
INCLUDES
==========================================*/

/*== Implement the Custom Header feature. ==*/

require get_template_directory() . '/inc/custom-header.php';

/*== Custom template tags for this theme. ==*/

require get_template_directory() . '/inc/template-tags.php';

/*== Custom functions that act independently of the theme templates. ==*/

require get_template_directory() . '/inc/extras.php';

/*== Customizer additions. ==*/

require get_template_directory() . '/inc/customizer.php';

/*== Load Jetpack compatibility file. ==*/

require get_template_directory() . '/inc/jetpack.php';
