<?php
/*
Template Name: News
*/

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page_header">

				<!-- Header Video -->

				<?php if(get_field('header_video_mp4')): ?>
					<video id="video" autoplay loop="true" poster="<?php the_field('header_video_poster_image'); ?>" width="100%" height="auto">
						<source src="<?php the_field('header_video_mp4'); ?>" type="video/mp4">
						<source src="<?php the_field('header_video_webm'); ?>" type="video/webm">
						<source src="<?php the_field('header_video_ogg'); ?>" type="video/ogg">
					</video>
					<img class="mobile_banner" src="<?php the_field('jl_header_image'); ?>" />
				<?php else: ?>
					<img src="<?php the_field('jl_header_image'); ?>" />
				<?php endif; ?>
			</header>

			<div class="page_content">
				<div id="news_container">
					<h2 class="center">News</h2>

					<!-- News Filters -->
					
					<ul id="mixitup_filters">
						<?php

						// Loop through all of the post categories to add them as filters. Exclude the blog category and all of its children.

						$args = array('taxonomy' => 'category', 'exclude_tree' => array(13));
						$categories = get_categories( $args );
						foreach($categories as $cat){ 
						?>	
							<?php 
							// Don't add the 'uncategorized' filter.
							if($cat->name !== 'Uncategorized'): ?>
								<li class="filter" data-filter="<?php $jl_filter_category_name = $cat->name; $jl_filter_category_name = preg_replace('/\s*/', '', $jl_filter_category_name); $jl_filter_category_name = strtolower($jl_filter_category_name); echo '.' . $jl_filter_category_name;  ?>"><?php echo $cat->name ?></li>
							<?php endif; ?>
						<?php } ?>
					</ul>

					<!-- News Items -->

					<div id="news_mixitup" class="mixitup_container">
						<?php

						// Loop through all of the Posts and attach their categories as classes. Do not show posts within the 'Blog' category.

						$news_args = array('post_type' => 'post', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date', 'category__not_in' => array(13));
						$news_loop = new WP_Query($news_args);
						if ( $news_loop->have_posts() ) : while ( $news_loop->have_posts() ) : $news_loop->the_post();
						?>
							<div class="mix <?php foreach((get_the_category()) as $category) { $jl_category_name = $category->cat_name; $jl_category_name = preg_replace('/\s*/', '', $jl_category_name); $jl_category_name = strtolower($jl_category_name); echo $jl_category_name . ' '; } ?>">
								<div class="jl_overlay_container">
									<?php the_post_thumbnail(); ?>
									<div class="jl_overlay">
										<a href="<?php the_permalink(); ?>"></a>
										<h2><?php the_title(); ?></h2>
										<?php foreach((get_the_category()) as $category) { ?>
											<?php 
											
											// If category is 'uncategorized' then don't show the category name.

											if($category->name !== 'Uncategorized'): ?>
												<span class="category"><?php echo $category->cat_name . ' '; ?></span>
											<?php endif; ?>
										<?php } ?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
						<?php endif; wp_reset_postdata(); ?>
					</div>

					<!-- Show More -->

					<div class="loadmore">
						<div class="loadmore-button"></div>
					</div>

				</div>
				<?php get_template_part( 'template-parts/instagram' ); ?>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>