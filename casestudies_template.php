<?php
/*
Template Name: Case Studies
*/

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content">
				<div class="flex_container_grid">
					
					<?php
					$casestudies_args = array('post_type' => 'jl_casestudies', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date');
					$casestudies_loop = new WP_Query($casestudies_args);
					if ( $casestudies_loop->have_posts() ) : while ( $casestudies_loop->have_posts() ) : $casestudies_loop->the_post();
					?>
						
						<div class="case_study_thumbnail">
							<div class="jl_overlay_container">
								<?php the_post_thumbnail(); ?>
								<div class="jl_overlay">
									<h2><?php the_title(); ?></h2>
									<a href="<?php the_permalink(); ?>"></a>
									<?php if(get_the_terms($post->id, 'case_studies_category')): ?>
										<?php foreach((get_the_terms($post->id, 'case_studies_category')) as $category) { ?>
											<span class="category"><?php echo $category->name . ' '; ?></span>
										<?php } ?>
									<?php endif; ?>
								</div>
							</div>
						</div>

					<?php endwhile; endif; wp_reset_postdata(); ?>

				</div>
				<?php get_template_part( 'template-parts/instagram' ); ?>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>