<ul class="jl_social_share">
	<li><span class="share_title">Share</span></li>
	<?php $title = get_the_title(); $link = get_the_permalink(); ?>
	<li><a href="mailto:?subject=<?php echo $title; ?>&amp;body=<?php echo $link; ?>">Email</a></li>
	<li><a class="fb_share" href="<?php the_permalink(); ?>">Facebook</a></li>
	<li><a class="twitter_share" href="<?php the_permalink(); ?>">Twitter</a></li>
</ul>

<div class="jl_post_navigation">
	<?php $prev_post = get_previous_post(); 
	if(!empty($prev_post)): ?>
		<a class="prev_post" href="<?php echo get_permalink($prev_post) ?>">
			<img src="<?php echo get_template_directory_uri() . '/img/arrow-previous.svg'; ?>" /> 
			<span>Previous</span>
		</a>
	<?php else: ?>
		<a href=""></a>
	<?php endif; ?>

	<?php $next_post = get_next_post(); 
	if(!empty($next_post)): ?>
		<a class="next_post" href="<?php echo get_permalink($next_post) ?>">
			<span>Next</span> 
			<img src="<?php echo get_template_directory_uri() . '/img/arrow-next.svg'; ?>" />
		</a>
	<?php else: ?>
		<a href=""></a>
	<?php endif; ?>
</div>