<?php
/*
Template Name: Blog
*/

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content">
				<h2 class="center">Blog</h2>

				<!-- Blog Filters -->
				
				<ul id="mixitup_filters">
					<li class="filter" data-filter="all">All</li>
					<?php

					// Loop through all of the 'Blog' taxonomy child terms to add them as filters.

					$args = array('taxonomy' => 'category', 'child_of' => 13);
					$categories = get_categories( $args );
					foreach($categories as $cat){ 
					?>	
						<?php 
						// Don't add the 'uncategorized' filter.
						if($cat->name !== 'Uncategorized'): ?>
							<li class="filter" data-filter="<?php $jl_filter_category_name = $cat->name; $jl_filter_category_name = preg_replace('/\s*/', '', $jl_filter_category_name); $jl_filter_category_name = strtolower($jl_filter_category_name); echo '.' . $jl_filter_category_name;  ?>"><?php echo $cat->name ?></li>
						<?php endif; ?>
					<?php } ?>
				</ul>

				<!-- Blog Items -->

				<div id="blog_mixitup" class="mixitup_container">
					<?php

					// Loop through all of the posts within the Blog category.

					$news_args = array('post_type' => 'post', 'posts_per_page' => -1, 'order' => 'DSC', 'order_by' => 'date', 'category__in' => array(13));
					$news_loop = new WP_Query($news_args);
					if ( $news_loop->have_posts() ) : while ( $news_loop->have_posts() ) : $news_loop->the_post();
					?>
						<div class="mix <?php foreach((get_the_category()) as $category) { $jl_category_name = $category->cat_name; $jl_category_name = preg_replace('/\s*/', '', $jl_category_name); $jl_category_name = strtolower($jl_category_name); echo $jl_category_name . ' '; } ?>">
							<div class="jl_overlay_container">
								<?php the_post_thumbnail(); ?>
								<div class="jl_overlay">
									<a href="<?php the_permalink(); ?>"></a>
									<h2><?php the_title(); ?></h2>
									<?php foreach((get_the_category()) as $category) { ?>
										<?php 
										
										// Don't show the term names for 'uncategorized' or 'blog.'

										if(($category->name !== 'Blog') && ($category->name !== 'Uncategorized')): ?>
											<span class="category"><?php echo $category->cat_name . ' '; ?></span>
										<?php endif; ?>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
					<?php endif; wp_reset_postdata(); ?>
				</div>

				<!-- Show More -->

				<div class="loadmore">
					<div class="loadmore-button"></div>
				</div>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
