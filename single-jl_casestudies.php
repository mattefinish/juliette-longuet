<?php
/**
 * The template for displaying all single posts from a specific custom post types.
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="page_header">
				<img src="<?php the_field('jl_header_image'); ?>" />
				<div class="header_content">
					<h1><?php the_title(); ?></h1>
					<?php if(get_the_terms($post->id, 'case_studies_category')): ?>
						<?php foreach((get_the_terms($post->id, 'case_studies_category')) as $category) { ?>
							<span class="category"><?php echo $category->name . ' '; ?></span>
						<?php } ?>
					<?php endif; ?>
				</div>
			</header>

			<div class="page_content">
				<?php the_content(); ?>
				<?php get_template_part( 'template-parts/singlepost_navigation_social' ); ?>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>