<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content">
				<div class="news_content_container">
					<header class="news_header">
						<div class="featured_image">
							<?php the_post_thumbnail(); ?>
						</div>
					</header>
					<section class="news_title">
						<h2><?php the_title(); ?></h2>
						<span class="news_date"><?php the_date('m.d.y'); ?></span>
					</section>
					<section class="news_content">
						<?php the_content(); ?>
					</section>
				</div>

				<?php get_template_part( 'template-parts/singlepost_navigation_social' ); ?>
				
				<?php get_template_part( 'template-parts/instagram' ); ?>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
