<?php
/*
Template Name: Clients
*/

get_header(); ?>


	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content">
				<div class="flex_container_grid">
					<?php if( have_rows('jl_clients') ): ?>

						<?php while( have_rows('jl_clients') ): the_row(); ?>

							<img class="client_image" src="<?php the_sub_field('jl_clients_image'); ?>" />

						<?php endwhile; ?>

					<?php endif; ?>
				</div>
				<?php get_template_part( 'template-parts/instagram' ); ?>
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
