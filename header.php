<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- Favicon -->

	<link rel="icon" type="img/png" href="<?php echo get_template_directory_uri() . '/favicon.png'; ?>" />

	<?php wp_head(); ?>
</head>

<body <?php if(get_field('jl_header_image') || get_field('header_video_mp4')):
				body_class('header_image');
			else:
				body_class();
			endif;
	  ?>
>

<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>

	<!-- Mobile Navigation -->

    <div class="mobile_navigation">

    	<header class="mobile_header">
	    	<div class="mobile_header_container">
		    	<div class="mobile_logo_container">	
		    		<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php the_field('jl_black_logo','option'); ?>" /></a>
		    	</div>
		        <div class="close_menu_icon"></div>
		    </div>
	    </header>
        
        <div class="content_container">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'mobile-menu', 'container' => '' ) ); ?>
        </div>  

    </div>

	<!-- Mobile Header -->

	<div class="main_mobile_header">

	    <header class="mobile_header">
	    	<div class="mobile_header_container">
		    	<div class="mobile_logo_container">	
		    		<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php the_field('jl_black_logo','option'); ?>" /></a>
		    	</div>
		        <div class="menu_icon"></div>
		    </div>
	    </header>

	</div>

	<!-- Main Header -->

	<header class="site_header" role="banner">

		<nav class="main_navigation" role="navigation">
			<div class="logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img class="black_logo" src="<?php the_field('jl_black_logo','option'); ?>" />
				</a>
			</div>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
		</nav>

	</header>

	<div id="content" class="main_content">
